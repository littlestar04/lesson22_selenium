import homeBrand.CartPreview;
import homeBrand.MainCatalogue;
import homeBrand.OrderFormPage;
import homeBrand.TShirtCard;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckOrderForm {
    protected WebDriver driver;
    private MainCatalogue mainCatalogue;
    private OrderFormPage orderFormPage;
    private TShirtCard itemCard;
    private CartPreview cartPreview;

    @Before
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().fullscreen();
        mainCatalogue = new MainCatalogue(driver);
        orderFormPage = new OrderFormPage(driver);
        itemCard = new TShirtCard(driver);
        cartPreview = new CartPreview(driver);
    }
    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testOrderForm(){
        mainCatalogue.openHomePage().chooseItem();
        itemCard.addToCart().seeCart();
        cartPreview.checkOut();
        orderFormPage.waitFormToAppear()
                .fillFIO("Иванов Иван Иванович")
                .fillPhone("0000000000")
                .fillRegion("Армения")
                .fillAddress("Москва, улица Пушкина")
                .fillFullAddressCity()
                .chooseWayOfDelivery()
                .fillFIOForDelivery("Иванов Иван Иванович")
                .fillStreet("Пушкина")
                .fillFlat("9")
                .fillHouse("10")
                .toOrder()
                .errorMessage1()
                .errorMessage2();
    }
}
