import homeBrand.MainCatalogue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckLongsleeve {
    protected WebDriver driver;
    private MainCatalogue mainCatalogue;

    @Before
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().fullscreen();
        mainCatalogue = new MainCatalogue(driver);
    }
    @After
    public void tearDown() {
        driver.quit();
    }
    @Test
    public void testCheckLongsleeve(){
        mainCatalogue.openHomePage()
                .chooseItemForSearch("Лонгслив White&Green")
                .searchItem()
                .waitPriceToAppear()
                .itemShouldBe()
                .quantityShouldBe()
                .priceShouldBe();
    }
}
