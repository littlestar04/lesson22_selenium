package homeBrand;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPreview {
    private WebDriver driver;
    @FindBy(xpath = "//button[@class='t706__sidebar-continue t-btn']")
    private WebElement checkout;
    public CartPreview(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public CartPreview checkOut(){
        checkout.click();
        return this;
    }
}
