package homeBrand;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class OrderFormPage {
    WebDriver driver;
    public OrderFormPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@id='input_1496239431201']")
    private WebElement FIO;

    @FindBy(xpath = "//*[@id='form561378404']/div[2]/div[2]/div/div[1]/input[2]")
    private WebElement phone;
    //@FindBy(xpath = "")
    //private WebElement phone;
    @FindBy(xpath = "//input[@id='input_1627385047591']")
    private WebElement regionName;
    @FindBy(xpath = "//textarea[@name='Адрес для доставки']")
    private WebElement address;
    @FindBy(xpath = "//input[@name='tildadelivery-city']")
    private WebElement fullAddressCity;
    @FindBy(xpath = "//div[@data-full-name='Россия, г Москва']")
    private WebElement fullAddressCityCountry;

    @FindBy(xpath = "//label[@data-service-id='1444328482']//div[@class='t-radio__indicator']")
    private WebElement deliveryMethod;
    @FindBy(xpath = "//input[@name='tildadelivery-userinitials']")
    private WebElement deliveryFIO;
    @FindBy(xpath = "//input[@name='tildadelivery-street']")
    private WebElement street;
    @FindBy(xpath = "//*[@id='street-searchbox']/div[1]/div[2]/div/div[1]")
    private WebElement listOfStreets;
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement house;
    @FindBy(xpath = "//input[@name='tildadelivery-aptoffice']")
    private WebElement flat;
    @FindBy(xpath = "//button[text()='ОФОРМИТЬ ЗАКАЗ' and @type='submit']")
    private WebElement makeAnOrder;
    @FindBy(xpath = "//*[@id='form561378404']/div[2]")
    private WebElement form;
    @FindBy(xpath = "//input[@name='tildadelivery-userinitials']")
    private WebElement FIOForDelivery;
    @FindBy(xpath = "//p[@class='t-form__errorbox-item js-rule-error js-rule-error-phone' and text()='Укажите, пожалуйста, корректный номер телефона']")
    private WebElement errorMessage1;
    @FindBy(xpath = "//div[@id='error_1496239478607']")
    private WebElement errorMessage2;


    public OrderFormPage waitFormToAppear(){
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(form));
        return this;
    }
    public OrderFormPage fillFIO(String fullName){
        FIO.sendKeys(fullName);
        return this;
    }
    public OrderFormPage fillPhone(String phoneNumber){
        phone.sendKeys(phoneNumber);
        return this;
    }
    public OrderFormPage fillRegion(String nameOfRegion){
        regionName.sendKeys(nameOfRegion);
        return this;
    }
    public OrderFormPage fillAddress(String addressForDelivery){
        address.sendKeys(addressForDelivery);
        return this;
    }
    public OrderFormPage fillFullAddressCity(){
        fullAddressCity.click();
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-full-name='Россия, г Москва']")));
        fullAddressCityCountry.click();
        return this;
    }

    public OrderFormPage chooseWayOfDelivery(){
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(deliveryMethod));
        deliveryMethod.click();
        return this;
    }
    public OrderFormPage fillFIOForDelivery(String fullName){
        FIOForDelivery.sendKeys(fullName);
        return this;
    }
    public OrderFormPage fillStreet(String streetName){
        street.sendKeys(streetName);
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='street-searchbox']/div[1]/div[2]/div/div[1]")));
        listOfStreets.click();
        return this;
    }
    public OrderFormPage fillFlat(String numberOfFlat){
        flat.sendKeys(numberOfFlat);
        return this;
    }
    public OrderFormPage fillHouse(String numberOfHouse){
        house.sendKeys(numberOfHouse);
        return this;
    }
    public OrderFormPage toOrder(){
        makeAnOrder.click();
        return this;
    }
    public OrderFormPage errorMessage1() {
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(errorMessage1));
        Assert.assertTrue(driver
                .findElement(By.xpath("//p[@class='t-form__errorbox-item js-rule-error js-rule-error-phone' and text()='Укажите, пожалуйста, корректный номер телефона']"))
                .getText().contains("Укажите, пожалуйста, корректный номер телефона"));
        return this;
    }
     public OrderFormPage errorMessage2(){
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(errorMessage2));
        Assert.assertTrue(driver
                .findElement(By.xpath("//div[@id='error_1496239478607']"))
                .getText().contains("Укажите, пожалуйста, корректный номер телефона"));
        return this;
    }

}
