package homeBrand;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TShirtCard {
    @FindBy(xpath = "//td[@class='js-store-prod-popup-buy-btn-txt']")
    private WebElement add;

    @FindBy(xpath = "//div[@class='t706__carticon-imgwrap']")
    private WebElement cart;


    private WebDriver driver;
    public TShirtCard(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public TShirtCard addToCart(){
        add.click();
        return this;
    }

    public TShirtCard seeCart(){
        cart.click();
        return this;
    }
}
