package homeBrand;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class MainCatalogue {
    @FindBy(xpath = "//input[@class='t-store__filter__input js-store-filter-search']")
    private WebElement searchLine;
    @FindBy(xpath = "//*[contains(@class, 'filter-search-btn')]")
    private WebElement search;

    @FindBy(xpath = "//a[@href='https://homebrandofficial.ru/popular/tproduct/561535806-930803998551-futbolka-oversize']")
    private WebElement tShirt;
    @FindBy(xpath = "//div[@data-product-price-def='2800']")
    private WebElement price;
    @FindBy(xpath = "//div[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md']")
    private WebElement item;
    @FindBy(xpath = "//span[@class='js-store-filters-prodsnumber']")
    private WebElement quantity;

    private final String baseUrl = "https://homebrandofficial.ru/wear";
    private WebDriver driver;
    public MainCatalogue(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public MainCatalogue openHomePage(){
        driver.get(baseUrl);
        return this;
    }
    public MainCatalogue chooseItemForSearch(String itemName){
        searchLine.sendKeys(itemName);
        return this;
    }
    public MainCatalogue searchItem(){
        search.click();
        return this;
    }
    public MainCatalogue chooseItem(){
        tShirt.click();
        return this;
    }
    public MainCatalogue waitPriceToAppear(){
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(price));
        return this;
    }

    public MainCatalogue priceShouldBe(){
        Assert.assertTrue(driver.findElement(By.xpath("//div[@data-product-price-def='2800']"))
                .getText().contains("2 800"));
        return this;
    }


    public MainCatalogue itemShouldBe(){
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md']"))
                .getText().contains("Лонгслив White&Green"));
        return this;
    }
    public MainCatalogue quantityShouldBe(){
        Assert.assertTrue(driver
                .findElement(By.xpath("//span[@class='js-store-filters-prodsnumber']"))
                .getText().contains("1"));
        return this;
    }


}
